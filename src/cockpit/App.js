import React, {Component} from 'react';
import './App.scss';
import Axios from "axios";
import InfiniteScroll from "react-infinite-scroll-component";
import {authToken} from "../constants";


class App extends Component {

    constructor(props) {
        super(props);


        this.state = {
            layout: 'twoBytwo',
            searchString: '',
            pagination: 1,
            imageData: null
        }

    }


    handleLayoutChange = (e) => {

        this.setState({
            layout: e.target.value
        });

    };


    searchByString = () => {

        if (localStorage.getItem(this.state.searchString)) {

            const dataToBePrinted = JSON.parse(localStorage.getItem(this.state.searchString));

            let pagination = 1;

            if (localStorage.getItem(`${this.state.searchString}page`)) {
                pagination = parseInt(localStorage.getItem(`${this.state.searchString}page`));
            }


            this.setState({
                imageData: dataToBePrinted,
                pagination
            })


        } else {

            this.setState({
                pagination: 1,
                imageData: null
            })

            Axios.get(`https://api.imgur.com/3/gallery/search/1?q=${this.state.searchString}`, {
                headers: {
                    'Authorization': `Bearer ${authToken}`
                }
            })
                .then(res => {
                    localStorage.setItem(`${this.state.searchString}`, JSON.stringify(res.data.data));
                    this.setState({
                        imageData: res.data.data
                    });
                })
                .catch(err => console.log(err));
        }


    }


    infiniteScrollApi = async () => {


        await this.setState({
            pagination: this.state.pagination + 1
        })


        setTimeout(() => {
            localStorage.setItem(`${this.state.searchString}page`, `${this.state.pagination}`)

            Axios.get(`https://api.imgur.com/3/gallery/search/${this.state.pagination}?q=${this.state.searchString}`, {
                headers: {
                    'Authorization': `Bearer ${authToken}`
                }
            })
                .then(res => {
                    localStorage.setItem(`${this.state.searchString}`, JSON.stringify([...this.state.imageData, ...res.data.data]));

                    this.setState({
                        imageData: [...this.state.imageData, ...res.data.data]
                    })


                })
                .catch(err => console.log(err));
        }, 1500)


    }


    render() {
        return (

            <div className='app__container'>

                <div className='application-container container-fluid'>

                    <div className='app__innerContainer'>


                        <div className='app__search'>
                            <div className='app__searchBox'>
                                <ion-icon name="search-outline"/>
                                <input onChange={(e) => this.setState({searchString: e.target.value})}
                                       value={this.state.searchString} type={'text'}/>
                            </div>
                            <div onClick={this.searchByString}
                                 className={`app__searchBtn ${this.state.searchString === '' ? 'disabled' : ''}`}>Search
                            </div>
                        </div>


                        <div className='app__layout'>
                            <input onChange={this.handleLayoutChange} type="radio" id="2x2"
                                   checked={this.state.layout === 'twoBytwo'} name="layout"
                                   value="twoBytwo"/>
                            <label htmlFor="2x2">2x2</label><br/>

                            <input onChange={this.handleLayoutChange} checked={this.state.layout === 'threeBythree'}
                                   type="radio"
                                   id="3x3" name="layout"
                                   value="threeBythree"/>
                            <label htmlFor="3x3">3x3</label>


                            <input onChange={this.handleLayoutChange} checked={this.state.layout === 'fourByfour'}
                                   type="radio"
                                   id="4x4" name="layout"
                                   value="fourByfour"/>
                            <label htmlFor="4x4">4x4</label>
                        </div>


                    </div>


                    {
                        this.state.imageData && <div className={`app__gridBox ${this.state.layout}`}>
                            <InfiniteScroll
                                dataLength={this.state.imageData.length}
                                next={this.infiniteScrollApi}
                                hasMore={true}
                                loader={<h4>Loading...</h4>}
                            >
                                {this.state.imageData && this.state.imageData.map((image, index) => {


                                    if (image.images) {

                                        if (image.images[0].type !== "video/mp4") {
                                            return <div className='app__gridBox-image'>
                                                <img key={index} src={image.images[0].link}/>
                                            </div>
                                        }

                                    }


                                })}

                            </InfiniteScroll>

                        </div>
                    }


                </div>


            </div>


        );
    }

}

export default App;
